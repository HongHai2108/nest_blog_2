import { Module } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Blog } from './blog.model/blog.model';

@Module({
  imports: [SequelizeModule.forFeature([Blog])],
  providers: [BlogService],
  controllers: [BlogController]
})
export class BlogModule {}

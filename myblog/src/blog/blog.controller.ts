import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { Blog } from './blog.model/blog.model';
import { BlogService } from './blog.service';

@Controller('blog')
export class BlogController {
    constructor(
        private readonly blogService: BlogService
    ) { }

    @Get()
    async findAll() {
        return await this.blogService.findAll();
    }

    @Get(':id')
    async findOne(@Param('id') id) {
       return await this.blogService.findOne(id);
    }

    @Post()
    async create(@Body() blog: Blog) {
        return await this.blogService.create(blog);
    }

    @Put(':id')
    async update(@Param('id') id, @Body() blog: Blog) {
        return await this.blogService.update(id, blog);
    }

    @Delete(':id')
    delete(@Param('id') id) {
        return this.blogService.delete(id);
    }
}

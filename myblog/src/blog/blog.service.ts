import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Blog } from './blog.model/blog.model';

@Injectable()
export class BlogService {
    constructor(
        @InjectModel(Blog)
        private blogModel: typeof Blog,
    ) { }

    async findAll(): Promise<Blog[]> {
        return this.blogModel.findAll();
    }

    findOne(id: string): Promise<Blog> {
        return this.blogModel.findOne({ where: { id } });
    }

    async create(blog: Blog): Promise<Blog> {
        return this.blogModel.create({blog: blog});
    }

    async update(id, data) {
        const [numberOfAffectedRows, [updatedPost]] = await this.blogModel.update({ ...data }, { where: { id }, returning: true });

        return { numberOfAffectedRows, updatedPost };
    }

    async delete(id: string) {
        return this.blogModel.destroy({ where: { id } });
    }
}

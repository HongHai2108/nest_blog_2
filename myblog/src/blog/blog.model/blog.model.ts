import { Column, Model, Table, PrimaryKey, ForeignKey } from 'sequelize-typescript';

@Table
export class Blog extends Model { 
    @PrimaryKey
    @Column
    id: number;

    @Column
    tittle: string;

    @Column
    description: string;

    @Column
    author: string;

    @Column
    category: string;
}

import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { Category } from './category.model/category.model';
import { CategoryService } from './category.service';

@Controller('category')
export class CategoryController {
    constructor(
        private readonly categoryService: CategoryService
    ) { }

    @Get()
    async findAll() {
        return await this.categoryService.findAll();
    }

    @Get(':id')
    async findOne(@Param('id') id) {
        return await this.categoryService.findOne(id);
    }

    @Post()
    async create(@Body() category: Category) {
        return await this.categoryService.create(category);
    }

    @Put(':id')
    async update(@Param('id') id, @Body() category: Category) {
        return await this.categoryService.update(id, category);
    }

    @Delete(':id')
    delete(@Param('id') id) {
        return this.categoryService.delete(id);
    }
}

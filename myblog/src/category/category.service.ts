import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Category } from './category.model/category.model';

@Injectable()
export class CategoryService {
    constructor(
        @InjectModel(Category)
        private categoryModel: typeof Category,
    ) { }

    async findAll(): Promise<Category[]> {
        return this.categoryModel.findAll();
    }

    findOne(id: string): Promise<Category> {
        return this.categoryModel.findOne({ where: { id } });
    }

    async create(Category: Category): Promise<Category> {
        return this.categoryModel.create({ Category: Category });
    }

    async update(id, data) {
        const [numberOfAffectedRows, [updatedPost]] = await this.categoryModel.update({ ...data }, { where: { id }, returning: true });

        return { numberOfAffectedRows, updatedPost };
    }

    async delete(id: string) {
        return this.categoryModel.destroy({ where: { id } });
    }
}

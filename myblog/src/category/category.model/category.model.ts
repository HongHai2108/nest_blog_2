import { Column, HasMany, Model, Table, PrimaryKey } from 'sequelize-typescript';

@Table
export class Category extends Model {
    @PrimaryKey
    @Column
    id: number;

    @Column
    name: string;
}
